#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "savefile.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

//Declaramos nuestra funcion/metodo slot
public slots:
    void cargarTexto();

private:
    Ui::MainWindow *ui;
    //Agregamos el widget fachada a nuestro widget principal
    SaveFile *save;
};
#endif // MAINWINDOW_H
