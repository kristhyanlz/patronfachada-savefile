#include "mainwindow.h"
#include "ui_mainwindow.h"

//Este sería el código cliente
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //El constructor tiene todos los parametros por defecto
    //Parametros: widget maestro, nombre del widget, posicion, texto del boton, texto a guardar
    save = new SaveFile(ui->centralwidget,
                        "Guardar",
                        QPoint(0,0),
                        "Texto Boton",
                        "Algun texto para guardar");
    //Modificamos los elementos constitutivos de la fachada
    save->setName("WidgetGuardar");
    save->move(50,50);
    save->setText("Guardar en fichero");
    save->setData("Este es el texto que deseo guardar");
    save->setGeometry(save->pos().x(), save->pos().y(), 120, 20);

    //Conectamos la señal de click del boton OK con el slot cargarTexto que es un método propio
    connect(ui->btnOk,SIGNAL(clicked()),this, SLOT(cargarTexto()));
}

//Se lee la cadena del plainTextEdit y lo pasamos a nuestra fachada por medio de setData
void MainWindow::cargarTexto(){
    save->setData(ui->plainTextEdit->toPlainText());
}

MainWindow::~MainWindow(){
    delete ui;
}
