#ifndef SAVEFILE_H
#define ENDL "\n"
#define SAVEFILE_H


#include <QWidget>
#include <QFileDialog>
#include <QPushButton>
#include <QMainWindow>
#include <QString>
#include <QObject>
#include <QFile>
#include <QTextStream>
#include <string>

#include <QDebug>

class SaveFile:public QWidget{
    Q_OBJECT
private:
    QString name;
    QPushButton *boton;
    QFileDialog *dialogo;
    QString data;

public:
    //El constructor tiene todos los parametros por defecto para que este también sea el constructor por defecto
    SaveFile(QWidget *parent = nullptr,
             const QString &name_=QString(),
             QPoint pos_ = QPoint(0,0),
             const QString &txtBoton = QString(),
             const QString &data_ = QString());

    //Los métodos inline son declarados en el header
    inline QPoint pos(){
        return boton->pos();
    }
    inline QRect geometry(){
        return boton->geometry();
    }
    inline void setParent(QWidget *parent){
        boton->setParent(parent);
    }
    inline void setName(const QString &name){
        this->name = name;
    }
    inline void setGeometry(int x, int y, int w, int h){
        boton->setGeometry(x, y, w, h);
    }

    inline void setText(const QString &txt){
         boton->setText(txt);
    }

    inline void setData(const QString &data_){
        data=data_;
    }
    inline QString getName(){
        return name;
    }
    inline void move(QPoint pos_){
        boton->move(pos_.x(), pos_.y());
    }
    inline void move(int x, int y){
        boton->move(x, y);

    }
//Para crear un método ranura debe ser parte de public slots
public slots:
    void play();

};

#endif // SAVEFILE_H
