#include "savefile.h"

SaveFile::SaveFile(QWidget *parent,
                   const QString &name_,
                   QPoint pos_,
                   const QString &txtBoton,
                   const QString &data_)
    : QWidget(parent), name(name_), data(data_)
{
    boton = new QPushButton(txtBoton, this);
    dialogo = new QFileDialog(this);
    boton->move(pos_.x(), pos_.y());
    //Conectamos el disparador clicked del boton a nuestro metodo play
    QObject::connect(boton, SIGNAL(clicked()), this, SLOT(play()));

}

//Por medio de la fachada simplificamos el accionar del boton guardar para que realice todo el proceso de guardado de forma automática
void SaveFile::play(){
    if (dialogo->exec()== QDialog::Accepted){//Verificamos si el usuario escogió un fichero
        QString filename = dialogo->selectedFiles().first();//Obtenemos el path completo
        QFile file(filename);//Objeto que realizará la escritura en el fichero, propio de Qt
        if (file.open(QIODevice::ReadWrite)){
            QTextStream stream(&file);
            stream << data << ENDL;
        }else{
            qDebug()<<"No se puede abrir el fichero!";//Imprimimos por consola (log) el error
        }
    }
}
